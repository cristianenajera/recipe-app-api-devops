terraform {
  backend "s3" {
    bucket         = "test-terraform-375588229801"
    key            = "app.tfstate"
    region         = "ca-central-1"
    encrypt        = true
    dynamodb_table = "test-terraform"
  }
}

provider "aws" {
  region  = "ca-central-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}