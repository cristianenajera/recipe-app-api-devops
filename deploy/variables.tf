variable "prefix" {
  default = "app-devops"
}

variable "project" {
  default = "app"
}

variable "contact" {
  default = "Convergence"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "375588229801.dkr.ecr.ca-central-1.amazonaws.com/test-terraform:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "375588229801.dkr.ecr.ca-central-1.amazonaws.com/test-ecr:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
